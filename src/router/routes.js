import LoginPage from "../components/routes/LoginPage";
import TaskTable from "../components/TaskTable";
import Register from "../components/routes/Register";

const routes = [
    {path: '/', name:'TaskTable', component: TaskTable},
    {path: '/login/:status', name: 'login', component: LoginPage},
    {path: '/register', name: 'register', component: Register}
];

export default routes;